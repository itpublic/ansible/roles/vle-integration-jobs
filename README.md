Ansible Role: vle-integration-jobs
=========

Enable, disable or check if canvas related integration jobs are paused and finished.

Requirements
------------

You need a working installation of canvas on the hosts your playbook is manipulating.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      tasks:
        - include_role:
            name: vle-integration-jobs
            tasks_from: check_if_finished

License
-------

MIT


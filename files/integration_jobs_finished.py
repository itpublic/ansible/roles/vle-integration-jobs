#!/usr/bin/python
import argparse
from collections import deque


def get_last_n_lines_from_file(f, n):
    with open(f) as g:
        return deque(g, n)


def get_all_lines_containing_substring(s, l):
    return list(filter(lambda x: s in x, l))


def get_last_job_number_from_list(l):
    return int(l[-1].partition(":")[2].partition("-")[0])


def maintenance(l):
    s = 'maintenance'
    return bool(get_all_lines_containing_substring(s, l))


def job_finished(file, start='Start', finish='Finish'):
    l0 = get_last_n_lines_from_file(file, 20)
    m0 = maintenance(list(l0)[-3:])
    if m0:
        return True

    l1 = get_all_lines_containing_substring(start, l0)
    l2 = get_all_lines_containing_substring(finish, l0)
    n1 = get_last_job_number_from_list(l1)
    n2 = get_last_job_number_from_list(l2)
    return n1 == n2


# Construct the argument parser
ap = argparse.ArgumentParser(
    prog='integration_jobs_paused',
    formatter_class=argparse.RawDescriptionHelpFormatter
)

ap.add_argument("--file", required=True,
                help="give the name of the logfile to read.")

args = vars(ap.parse_args())

print(job_finished(args['file']))

